from models.models import predictIMCModel


def verify_data_imc_model(params):
    real_params = {
        "Abdomen": 0,
        "Height": 0, 
        "Hip": 0, 
        "Thigh": 0, 
        "Weight": 0
    }

    real_params_list = []

    result_object = {
        "model": "imc",
        "params": params,
        "recognized": {},
        "missing": {},
        "errors": {},
        "response": None
    }

    #Abdomen
    abdomen = params.get("abdomen", None)
    if(abdomen != None):
        try:
            abdomen_number = float(abdomen)
            real_params['Abdomen'] = abdomen_number
            real_params_list.append(abdomen_number)
            result_object['recognized']['abdomen'] = abdomen
        except ValueError:
            result_object['errors']['abdomen'] = "Abdomen debe ser un número"
    else:
        result_object['missing']['abdomen'] = "Medida del abdomen en cm"
    
    #Height
    height = params.get("altura", None)
    if(height != None):
        try:
            height = height.replace("uno", "1")
            height_number = float(height)
            real_params['Height'] = height_number
            real_params_list.append(height_number)
            result_object['recognized']['altura'] = str(height_number)
        except ValueError:
            result_object['errors']['altura'] = "Altura debe ser un número"
    else:
        result_object['missing']['altura'] = "Medida de la altura en pulgadas"
    
    #Hip
    hip = params.get("cadera", None)
    if(hip != None):
        try:
            hip_number = float(hip)
            real_params['Hip'] = hip_number
            real_params_list.append(hip_number)
            result_object['recognized']['cadera'] = hip
        except ValueError:
            result_object['errors']['cadera'] = "Cadera debe ser un número"
    else:
        result_object['missing']['cadera'] = "Medida de la cadera en cm"

    #Thigh
    thigh = params.get("muslo", None)
    if(thigh != None):
        try:
            thigh_number = float(thigh)
            real_params['Thigh'] = thigh_number
            real_params_list.append(thigh_number)
            result_object['recognized']['muslo'] = thigh
        except ValueError:
            result_object['errors']['muslo'] = "Muslo debe ser un número"
    else:
        result_object['missing']['muslo'] = "Medida del muslo en cm"

    #Thigh
    weight = params.get("peso", None)
    if(weight != None):
        try:
            weight_number = float(weight)
            real_params['Weight'] = weight_number
            real_params_list.append(weight_number)
            result_object['recognized']['peso'] = weight
        except ValueError:
            result_object['errors']['peso'] = "Peso debe ser un número"
    else:
        result_object['missing']['peso'] = "Peso de la persona en libras"      


    if len(real_params_list) == 5:
        result_object['response'] = str(predictIMCModel(real_params))

    return result_object