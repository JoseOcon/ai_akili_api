from models.models import predictRossmanModel

def verify_data_rossman_model(params):
    real_params = []

    result_object = {
        "model": "rossman",
        "params": params,
        "recognized": {},
        "missing": {},
        "errors": {},
        "response": None
    }

    # date
    date = params.get("fecha", None)
    if(date != None):
        try:
            months = ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre']

            month = date.split(' del ')[0].replace(" ", "")
            year = date.split(' del ')[1].replace(" ", "")

            month = str(months.index(month)+1) if (months.index(month)+1) > 10 else  "0"+str(months.index(month)+1)
            year = str(int(year))
            
            real_date =  year + "-" + month + "-" + "01"
            real_params.append(real_date)
            result_object['recognized']['fecha'] = date
        except ValueError:
            result_object['errors']['fecha'] = "El formato de la fecha es *mes* del *año*"
        except: 
            result_object['errors']['fecha'] = "El formato de la fecha es *mes* del *año*"
    else:
        result_object['missing']['fecha'] = "Especifica la fecha ej: 'enero del 2022'"
    
   
    if len(real_params) == 1:
        result_object['response'] = str(predictRossmanModel([real_params]))

    return result_object