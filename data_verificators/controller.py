from data_verificators.stroke_model_data_verificator import verify_data_stroke_model
from data_verificators.telcom_model_data_verificator import verify_data_telcom_model
from data_verificators.cirrhosis_model_data_verificator import verify_data_cirrhosis_model
from data_verificators.covid_19_model_data_verfificator import verify_data_covid_model
from data_verificators.rossman_sales_model_data_verificator import verify_data_rossman_model
from data_verificators.imc_model_data_verificator import verify_data_imc_model
from data_verificators.hepatitis_model_data_verificator import verify_data_hepatitis_model
from data_verificators.btc_price_data_verificator import verify_data_btc_model
from data_verificators.avocado_price_data_verificator import verify_data_avocado_model
from data_verificators.standard_and_poors_data_verificator import verify_data_standard_and_poors_model

## Aquí se controlan los parámetros que envía y recibe cada modelo

def main_controller(what_they_want, previous_response_object = None):

    model = what_they_want['model'] if what_they_want['model'] != None else previous_response_object.get('model')

    params = what_they_want['params']

    if previous_response_object != None:
        params = {**previous_response_object.get('params', {}) , **what_they_want['params']} 
    
    if(model == None):
        return {"model": model, "params": params}

    functions_to_models = {
        "stroke": verify_data_stroke_model,
        "telcom": verify_data_telcom_model,
        "cirrhosis": verify_data_cirrhosis_model,
        "covid": verify_data_covid_model,
        "btc": verify_data_btc_model,
        "avocado": verify_data_avocado_model,
        "standard_and_poors": verify_data_standard_and_poors_model,
        "rossman": verify_data_rossman_model,
        "imc": verify_data_imc_model,
        "hepatitis": verify_data_hepatitis_model
    }

    return functions_to_models[model](params)

            



