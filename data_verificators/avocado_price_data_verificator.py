from models.models import predictAvocadoModel


def verify_data_avocado_model(params):
    real_params = []

    result_object = {
        "model": "avocado",
        "params": params,
        "recognized": {},
        "missing": {},
        "errors": {},
        "response": None
    }

    # date
    date = params.get("fecha", None)
    if(date != None):
        try:
            months = ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre']

            day = date.split('de')[0].replace(" ", "")
            month = date.split(' de ')[1].split(' del ')[0].replace(" ", "")
            year = date.split(' de ')[1].split(' del ')[1].replace(" ", "")

            if(day == "primero" or day == "uno"):
                day = '01'
            else: 
                day = str(int(day)) if int(day) > 10 else  "0"+str(int(day))

            month = str(months.index(month)+1) if (months.index(month)+1) > 10 else  "0"+str(months.index(month)+1)
            year = str(int(year))
            
            real_date =  year + "-" + month + "-" + day
            real_params.append(real_date)
            result_object['recognized']['fecha'] = date
        except ValueError:
            result_object['errors']['fecha'] = "El formato de la fecha es *día* de *mes* del *año*"
        except: 
            result_object['errors']['fecha'] = "El formato de la fecha es *día* de *mes* del *año*"
    else:
        result_object['missing']['fecha'] = "Especifica la fecha ej: '01 de enero del 2022'"
    
   
    if len(real_params) == 1:
        result_object['response'] = str(predictAvocadoModel([real_params]))

    return result_object