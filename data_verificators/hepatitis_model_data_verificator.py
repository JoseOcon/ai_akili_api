from models.models import predictHepatitisModel


def verify_data_hepatitis_model(params):
    real_params = {
        "Age": 0,
        "Sex": 0,
        "ALB": 0,
        "ALP": 0,  
        "ALT": 0,  
        "AST": 0,  
        "BIL": 0,  
        "CHE": 0,  
        "CHOL": 0, 
        "CREA": 0, 
        "GGT": 0,  
        "PROT": 0, 
    }

    real_params_list = []

    result_object = {
        "model": "hepatitis",
        "params": params,
        "recognized": {},
        "missing": {},
        "errors": {},
        "response": None
    }

    # age
    age = params.get("edad", None)
    if(age != None):
        try:
            age_number = int(age)
            real_params["Age"] = age_number
            real_params_list.append(age_number)
            result_object['recognized']['edad'] = age
        except ValueError:
            result_object['errors']['edad'] = "Edad debe ser un número natural"
    else:
        result_object['missing']['edad'] = "Edad del paciente"
    
    # sex
    sex = params.get("sexo", None)
    posibble_values = ['mujer' ,'hombre']
    if(sex != None):
        try:
            value = posibble_values.index(sex) + 1
            real_params["Sex"] = value
            real_params_list.append(value)
            result_object['recognized']['sexo'] = sex
        except ValueError:
            result_object['errors']['sexo'] = "Sexo solo puede ser " + " | ".join(posibble_values)
    else:
        result_object['missing']['sexo'] = "Sexo del paciente (" + " | ".join(posibble_values) + ")"

    # alb 
    alb = params.get("albúmina", None)
    if(alb != None):
        try:
            alb_number = float(alb)
            real_params['ALB'] = alb_number
            real_params_list.append(alb_number)
            result_object['recognized']['albúmina'] = alb
        except ValueError:
            result_object['errors']['albúmina'] = "La albúmina debe ser un número"
    else:
        result_object['missing']['albúmina'] = "La cantidad de albúmina en la sangre del paciente"    
    
    # alp 
    alp = params.get("fosfatasa", None)
    if(alp != None):
        try:
            alp_number = float(alp)
            real_params['ALP'] = alp_number
            real_params_list.append(alp_number)
            result_object['recognized']['fosfatasa'] = alp
        except ValueError:
            result_object['errors']['fosfatasa'] = "La fosfatasa alcalina debe ser un número"
    else:
        result_object['missing']['fosfatasa'] = "La cantidad de fosfatasa alcalina en la sangre del paciente" 

    # alt 
    alt = params.get("alanina", None)
    if(alt != None):
        try:
            alt_number = float(alt)
            real_params['ALT'] = alt_number
            real_params_list.append(alt_number)
            result_object['recognized']['alanina'] = alt
        except ValueError:
            result_object['errors']['alanina'] = "La alanina transaminasa debe ser un número"
    else:
        result_object['missing']['alanina'] = "La cantidad de alanina transaminasa en la sangre del paciente"

    # ast 
    ast = params.get("aspartato", None)
    if(ast != None):
        try:
            ast_number = float(ast)
            real_params['AST'] = ast_number
            real_params_list.append(ast_number)
            result_object['recognized']['aspartato'] = alt
        except ValueError:
            result_object['errors']['aspartato'] = "La aspartato transaminasa debe ser un número"
    else:
        result_object['missing']['aspartato'] = "La cantidad de aspartato transaminasa en la sangre del paciente"

    # bil 
    bil = params.get("bilirrubina", None)
    if(bil != None):
        try:
            bil_number = float(bil)
            real_params['BIL'] = bil_number
            real_params_list.append(bil_number)
            result_object['recognized']['bilirrubina'] = bil
        except ValueError:
            result_object['errors']['bilirrubina'] = "La bilirrubina debe ser un número"
    else:
        result_object['missing']['bilirrubina'] = "La cantidad de bilirrubina en la sangre del paciente"  

    # che 
    che = params.get("acetil", None)
    if(che != None):
        try:
            che_number = float(che)
            real_params['CHE'] = che_number
            real_params_list.append(che_number)
            result_object['recognized']['acetil'] = che
        except ValueError:
            result_object['errors']['acetil'] = "La acetilcolinesterasa debe ser un número"
    else:
        result_object['missing']['acetil'] = "La cantidad de acetilcolinesterasa en la sangre del paciente"

    # chol 
    chol = params.get("colesterol", None)
    if(chol != None):
        try:
            chol_number = float(chol)
            real_params['CHOL'] = chol_number
            real_params_list.append(chol_number)
            result_object['recognized']['colesterol'] = chol
        except ValueError:
            result_object['errors']['colesterol'] = "El colesterol debe ser un número"
    else:
        result_object['missing']['colesterol'] = "La cantidad de colesterol en la sangre del paciente"

    # crea 
    crea = params.get("creatinina", None)
    if(crea != None):
        try:
            crea_number = float(crea)
            real_params['CREA'] = crea_number
            real_params_list.append(crea_number)
            result_object['recognized']['creatinina'] = crea
        except ValueError:
            result_object['errors']['creatinina'] = "La creatinina debe ser un número"
    else:
        result_object['missing']['creatinina'] = "La cantidad de creatinina en la sangre del paciente"

    # ggt 
    ggt = params.get("glutamil", None)
    if(ggt != None):
        try:
            ggt_number = float(ggt)
            real_params['GGT'] = ggt_number
            real_params_list.append(ggt_number)
            result_object['recognized']['glutamil'] = ggt
        except ValueError:
            result_object['errors']['glutamil'] = "La gamma-glutamil transferasa debe ser un número"
    else:
        result_object['missing']['glutamil'] = "La cantidad de gamma-glutamil transferasa en la sangre del paciente"

    # prot 
    prot = params.get("proteína", None)
    if(prot != None):
        try:
            prot_number = float(prot)
            real_params['PROT'] = prot_number
            real_params_list.append(prot_number)
            result_object['recognized']['proteína'] = prot
        except ValueError:
            result_object['errors']['proteína'] = "La proteína deben ser un número"
    else:
        result_object['missing']['proteína'] = "La cantidad proteína del paciente"


    if len(real_params_list) == 12:
        result_object['response'] = str(predictHepatitisModel([[real_params["Age"], real_params["Sex"], real_params["ALB"], real_params["ALP"], real_params["ALT"], real_params["AST"], real_params["BIL"], real_params["CHE"], real_params["CHOL"], real_params["CREA"], real_params["GGT"], real_params["PROT"], ]]))

    return result_object