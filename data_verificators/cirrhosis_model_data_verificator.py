from models.models import predictCirrhosisModel


def verify_data_cirrhosis_model(params):
    real_params = []

    result_object = {
        "model": "cirrhosis",
        "params": params,
        "recognized": {},
        "missing": {},
        "errors": {},
        "response": None
    }

    # days
    days = params.get("días", None)
    if(days != None):
        try:
            days_number = int(days)
            real_params.append(days_number)
            result_object['recognized']['días'] = days
        except ValueError:
            result_object['errors']['días'] = "Días debe ser un número natural"
    else:
        result_object['missing']['días'] = "Número de días entre el registro y el más temprano entre la muerte, transplante o tiempo de análsis en 1986 (un número)"
    
    # Status
    status = params.get("estado", None)
    posibble_values = ['censurado' ,'censurado por hígado', "muerto"]
    if(status != None):
        try:
            value_index = posibble_values.index(status)
            real_params.append(value_index)
            result_object['recognized']['estado'] = status
        except ValueError:
            result_object['errors']['estado'] = "Estado solo puede ser " + " | ".join(posibble_values)
    else:
        result_object['missing']['estado'] = "Estado del paciente (" + " | ".join(posibble_values) + ")"

    # drug
    drug = params.get("tratamiento", None)
    posibble_values = ['penicilamina', 'placebo']
    if(drug != None):
        try:
            value_index = posibble_values.index(drug)
            real_params.append(value_index)
            result_object['recognized']['tratamiento'] = drug
        except ValueError:
            result_object['errors']['tratamiento'] = "tratamiento solo puede ser " + " | ".join(posibble_values)
    else:
        result_object['missing']['tratamiento'] = "tratamiento del paciente (" + " | ".join(posibble_values) + ")"

    # age
    age = params.get("edad", None)
    if(age != None):
        try:
            age_number = int(age)
            real_params.append(age_number)
            result_object['recognized']['edad'] = age
        except ValueError:
            result_object['errors']['edad'] = "Edad debe ser un número natural"
    else:
        result_object['missing']['edad'] = "Edad en días del paciente (un número)"

    # sex
    sex = params.get("sexo", None)
    posibble_values = ['mujer' ,'hombre']
    if(sex != None):
        try:
            value_index = posibble_values.index(sex)
            real_params.append(value_index)
            result_object['recognized']['sexo'] = sex
        except ValueError:
            result_object['errors']['sexo'] = "Sexo solo puede ser " + " | ".join(posibble_values)
    else:
        result_object['missing']['sexo'] = "Sexo del paciente (" + " | ".join(posibble_values) + ")"


    # ascites 
    ascites  = params.get("ascitis", None)
    if(ascites  != None):
        if(ascites  in ['sí', 'no', 'si']):
            real_params.append(0 if ascites  == 'no' else 1)
            result_object['recognized']['ascitis'] = ascites 
        else:
            result_object['errors']['ascitis'] = "Ascitis solo puede ser sí o no"
    else:
        result_object['missing']['ascitis'] = "Si el paciente presenta ascitis (sí o no)"
    

    # hepatomegaly  
    hepatomegaly = params.get("hepatomegalia", None)
    if(hepatomegaly != None):
        if(hepatomegaly in ['sí', 'no', 'si']):
            real_params.append(0 if hepatomegaly == 'no' else 1)
            result_object['recognized']['hepatomegalia'] = hepatomegaly
        else:
            result_object['errors']['hepatomegalia'] = "Hepatomegalia solo puede ser sí o no"
    else:
        result_object['missing']['hepatomegalia'] = "Si el paciente presenta Hepatomegalia (sí o no)"
    
    # spiders  
    spiders = params.get("arañas", None)
    if(spiders != None):
        if(spiders in ['sí', 'no', 'si']):
            real_params.append(0 if spiders == 'no' else 1)
            result_object['recognized']['arañas'] = spiders
        else:
            result_object['errors']['arañas'] = "Arañas solo puede ser sí o no"
    else:
        result_object['missing']['arañas'] = "Si el paciente presenta Arañas (sí o no)"


    # edema
    edema = params.get("edema", None)
    posibble_values = ['no' ,'sí', "sí con diuréticos"]
    if(edema != None):
        try:
            value_index = posibble_values.index(edema)
            real_params.append(value_index)
            result_object['recognized']['edema'] = edema
        except ValueError:
            result_object['errors']['edema'] = "Edema solo puede ser " + " | ".join(posibble_values)
    else:
        result_object['missing']['edema'] = "Edema del paciente (" + " | ".join(posibble_values) + ")"

    # bilirubin 
    bilirubin = params.get("bilirrubina", None)
    if(bilirubin != None):
        try:
            bilirubin_number = float(bilirubin)
            real_params.append(bilirubin_number)
            result_object['recognized']['bilirrubina'] = bilirubin
        except ValueError:
            result_object['errors']['bilirrubina'] = "La bilirrubina deben ser un número"
    else:
        result_object['missing']['bilirrubina'] = "La bilirrubina del paciente en mg/dl (número)"

    # cholesterol 
    cholesterol = params.get("colesterol", None)
    if(cholesterol != None):
        try:
            cholesterol_number = float(cholesterol)
            real_params.append(cholesterol_number)
            result_object['recognized']['colesterol'] = cholesterol
        except ValueError:
            result_object['errors']['colesterol'] = "El colesterol deben ser un número"
    else:
        result_object['missing']['colesterol'] = "El colesterol del paciente en mg/dl (número)"


    # albumin 
    albumin = params.get("albúmina", None)
    if(albumin != None):
        try:
            albumin_number = float(albumin)
            real_params.append(albumin_number)
            result_object['recognized']['albúmina'] = albumin
        except ValueError:
            result_object['errors']['albúmina'] = "La albúmina deben ser un número"
    else:
        result_object['missing']['albúmina'] = "La albúmina del paciente en mg/dl (número)"


    # copper 
    copper = params.get("cobre en orina", None)
    if(copper != None):
        try:
            copper_number = float(copper)
            real_params.append(copper_number)
            result_object['recognized']['cobre en orina'] = copper
        except ValueError:
            result_object['errors']['cobre en orina'] = "El cobre en orina deben ser un número"
    else:
        result_object['missing']['cobre en orina'] = "El cobre en orina del paciente en ug/día (número)"


    # alk_phos 
    alk_phos = params.get("fosfatasa alcalina", None)
    if(alk_phos != None):
        try:
            alk_phos_number = float(alk_phos)
            real_params.append(alk_phos_number)
            result_object['recognized']['fosfatasa alcalina'] = alk_phos
        except ValueError:
            result_object['errors']['fosfatasa alcalina'] = "La fosfatasa alcalina deben ser un número"
    else:
        result_object['missing']['fosfatasa alcalina'] = "La fosfatasa alcalina del paciente en u/litro (número)"


    # sgot 
    sgot = params.get("sgot", None)
    if(sgot != None):
        try:
            sgot_number = float(sgot)
            real_params.append(sgot_number)
            result_object['recognized']['sgot'] = sgot
        except ValueError:
            result_object['errors']['sgot'] = "El SGOT deben ser un número"
    else:
        result_object['missing']['sgot'] = "El SGOT del paciente en u/ml (número)"


    # triglycerides 
    triglycerides = params.get("triglicerios", None)
    if(triglycerides != None):
        try:
            triglycerides_number = float(triglycerides)
            real_params.append(triglycerides_number)
            result_object['recognized']['triglicerios'] = triglycerides
        except ValueError:
            result_object['errors']['triglicerios'] = "Los triglicerios deben ser un número"
    else:
        result_object['missing']['triglicerios'] = "Los triglicerios del paciente en mg/dl (número)"


    # platelets 
    platelets = params.get("plaquetas", None)
    if(platelets != None):
        try:
            platelets_number = float(platelets)
            real_params.append(platelets_number)
            result_object['recognized']['plaquetas'] = platelets
        except ValueError:
            result_object['errors']['plaquetas'] = "Las plaquetas deben ser un número"
    else:
        result_object['missing']['plaquetas'] = "Las plaquetas del paciente en ml/1000 cúbico (número)"


    # prothrombin 
    prothrombin = params.get("protrombina", None)
    if(prothrombin != None):
        try:
            prothrombin_number = float(prothrombin)
            real_params.append(prothrombin_number)
            result_object['recognized']['protrombina'] = prothrombin
        except ValueError:
            result_object['errors']['protrombina'] = "Protrombina deben ser un número"
    else:
        result_object['missing']['protrombina'] = "Tiempo de protrombina del paciente en segundos (número)"

    if len(real_params) == 18:
        result_object['response'] = str(predictCirrhosisModel([real_params]))

    return result_object