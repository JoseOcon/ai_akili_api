from models.models import predictTelcoChurnModel


def verify_data_telcom_model(params):
    real_params = []

    result_object = {
        "model": "telcom",
        "params": params,
        "recognized": {},
        "missing": {},
        "errors": {},
        "response": None
    }
    
    # gender
    gender = params.get("género", None)
    if(gender != None):
        if(gender in ['hombre','mujer']):
            real_params.append(1 if gender == 'hombre' else 0)
            result_object['recognized']['género'] = gender
        else:
            result_object['errors']['género'] = "Género solo puede ser hombre o mujer"
    else:
        result_object['missing']['género'] = "Género del paciente (hombre o mujer)"


    #SeniorCitizen
    senior_citizen = params.get("adulto mayor", None)
    if(senior_citizen != None):
        if(senior_citizen in ['sí', 'no', 'si']):
            real_params.append(0 if senior_citizen == 'no' else 1)
            result_object['recognized']['adulto mayor'] = senior_citizen
        else:
            result_object['errors']['adulto mayor'] = "Adulto mayor solo puede ser sí o no"
    else:
        result_object['missing']['adulto mayor'] = "Si el cliente es adulto mayor (sí o no)"
    

    # asociado
    partner = params.get("asociado", None)
    if(partner != None):
        if(partner in ['sí', 'no', 'si']):
            real_params.append(0 if partner == 'no' else 1)
            result_object['recognized']['asociado'] = partner
        else:
            result_object['errors']['asociado'] = "Asociado solo puede ser sí o no"
    else:
        result_object['missing']['asociado'] = "Si el cliente está asociado (sí o no)"


    # dependents
    dependents = params.get("dependiente", None)
    if(dependents != None):
        if(dependents in ['sí', 'no', 'si']):
            real_params.append(0 if dependents == 'no' else 1)
            result_object['recognized']['dependiente'] = dependents
        else:
            result_object['errors']['dependiente'] = "Dependiente solo puede ser sí o no"
    else:
        result_object['missing']['dependiente'] = "Si el cliente es dependiente (sí o no)"


    # tenure
    tenure = params.get("permanencia", None)
    if(tenure != None):
        try:
            tenure_number = int(tenure)
            real_params.append(tenure_number)
            result_object['recognized']['permanencia'] = tenure
        except ValueError:
            result_object['errors']['permanencia'] = "La permanencia debe ser un número natural"
    else:
        result_object['missing']['permanencia'] = "permanencia del paciente (un número)"

    # PhoneService
    phone_service = params.get("servicio telefónico", None)
    if(phone_service != None):
        if(phone_service in ['sí', 'no', 'si']):
            real_params.append(0 if phone_service == 'no' else 1)
            result_object['recognized']['servicio telefónico'] = phone_service
        else:
            result_object['errors']['servicio telefónico'] = "Servicio telefónico solo puede ser sí o no"
    else:
        result_object['missing']['servicio telefónico'] = "Si el cliente tiene servicio telefónico (sí o no)"
    

    # MultipleLines
    multiple_lines = params.get("múltiples líneas", None)
    posibble_values = ['no' ,'no tiene servicio telefónico', "sí"]
    if(multiple_lines != None):
        try:
            value_index = posibble_values.index(multiple_lines)
            real_params.append(value_index)
            result_object['recognized']['múltiples líneas'] = multiple_lines
        except ValueError:
            result_object['errors']['múltiples líneas'] = "Múltiples líneas solo puede ser " + " | ".join(posibble_values)
    else:
        result_object['missing']['múltiples líneas'] = "Si el cliente tiene múltiples líneas (" + " | ".join(posibble_values) + ")"


    # InternetService
    internet_service = params.get("servicio de internet", None)
    posibble_values = ['dsl' ,'fibra óptica', "no"]
    if(internet_service != None):
        try:
            value_index = posibble_values.index(internet_service)
            real_params.append(value_index)
            result_object['recognized']['servicio de internet'] = internet_service
        except ValueError:
            result_object['errors']['servicio de internet'] = "Servicio de internet solo puede ser " + " | ".join(posibble_values)
    else:
        result_object['missing']['servicio de internet'] = "Si el cliente tiene servicio de internet (" + " | ".join(posibble_values) + ")"


    # OnlineSecurity
    online_security = params.get("seguridad en línea", None)
    posibble_values = ['no' ,'no tiene servicio de internet', "sí"]
    if(online_security != None):
        try:
            value_index = posibble_values.index(online_security)
            real_params.append(value_index)
            result_object['recognized']['seguridad en línea'] = online_security
        except ValueError:
            result_object['errors']['seguridad en línea'] = "Seguridad en línea solo puede ser " + " | ".join(posibble_values)
    else:
        result_object['missing']['seguridad en línea'] = "Si el cliente tiene seguridad en línea (" + " | ".join(posibble_values) + ")"
  

    # OnlineBackup
    online_backup = params.get("respaldo en línea", None)
    posibble_values = ['no' ,'no tiene servicio de internet', "sí"]
    if(online_backup != None):
        try:
            value_index = posibble_values.index(online_backup)
            real_params.append(value_index)
            result_object['recognized']['respaldo en línea'] = online_backup
        except ValueError:
            result_object['errors']['respaldo en línea'] = "Respaldo en línea solo puede ser " + " | ".join(posibble_values)
    else:
        result_object['missing']['respaldo en línea'] = "Si el cliente tiene respaldo en línea (" + " | ".join(posibble_values) + ")"
  

    # DeviceProtection
    device_protection = params.get("protección a dispositivos", None)
    posibble_values = ['no' ,'no tiene servicio de internet', "sí"]
    if(device_protection != None):
        try:
            value_index = posibble_values.index(device_protection)
            real_params.append(value_index)
            result_object['recognized']['protección a dispositivos'] = device_protection
        except ValueError:
            result_object['errors']['protección a dispositivos'] = "protección a dispositivos solo puede ser " + " | ".join(posibble_values)
    else:
        result_object['missing']['protección a dispositivos'] = "Si el cliente tiene protección a dispositivos (" + " | ".join(posibble_values) + ")"
  

    # TechSupport
    tech_support = params.get("soporte de tecnología", None)
    posibble_values = ['no' ,'no tiene servicio de internet', "sí"]
    if(tech_support != None):
        try:
            value_index = posibble_values.index(tech_support)
            real_params.append(value_index)
            result_object['recognized']['soporte de tecnología'] = tech_support
        except ValueError:
            result_object['errors']['soporte de tecnología'] = "Soporte de tecnología solo puede ser " + " | ".join(posibble_values)
    else:
        result_object['missing']['soporte de tecnología'] = "Si el cliente tiene soporte de tecnología (" + " | ".join(posibble_values) + ")"
  

    # StreamingTV
    streaming_tv = params.get("transmisión de televisión", None)
    posibble_values = ['no' ,'no tiene servicio de internet', "sí"]
    if(streaming_tv != None):
        try:
            value_index = posibble_values.index(streaming_tv)
            real_params.append(value_index)
            result_object['recognized']['transmisión de televisión'] = streaming_tv
        except ValueError:
            result_object['errors']['transmisión de televisión'] = "Transmisión de televisión solo puede ser " + " | ".join(posibble_values)
    else:
        result_object['missing']['transmisión de televisión'] = "Si el cliente tiene transmisión de televisión (" + " | ".join(posibble_values) + ")"
  
    
    # StreamingMovies
    streaming_movies = params.get("transmisión de películas", None)
    posibble_values = ['no' ,'no tiene servicio de internet', "sí"]
    if(streaming_movies != None):
        try:
            value_index = posibble_values.index(streaming_movies)
            real_params.append(value_index)
            result_object['recognized']['transmisión de películas'] = streaming_movies
        except ValueError:
            result_object['errors']['transmisión de películas'] = "Transmisión de películas solo puede ser " + " | ".join(posibble_values)
    else:
        result_object['missing']['transmisión de películas'] = "Si el cliente tiene transmisión de películas (" + " | ".join(posibble_values) + ")"
  
    
    # Contract
    contract = params.get("contrato", None)
    posibble_values = ['mensual' ,'un año', "2 años"]
    if(contract != None):
        try:
            value_index = posibble_values.index(contract)
            real_params.append(value_index)
            result_object['recognized']['contrato'] = contract
        except ValueError:
            result_object['errors']['contrato'] = "Contrato solo puede ser " + " | ".join(posibble_values)
    else:
        result_object['missing']['contrato'] = "Tipo de contrato del cliente (" + " | ".join(posibble_values) + ")"
  
    
    # PaperlessBilling
    paperless_billing = params.get("facturación sin papel", None)
    posibble_values = ['mensual' ,'un año', "2 años"]
    if(paperless_billing != None):
        try:
            value_index = posibble_values.index(paperless_billing)
            real_params.append(value_index)
            result_object['recognized']['facturación sin papel'] = paperless_billing
        except ValueError:
            result_object['errors']['facturación sin papel'] = "Facturación sin papel solo puede ser " + " | ".join(posibble_values)
    else:
        result_object['missing']['facturación sin papel'] = "Si el cliente factura sin papel (" + " | ".join(posibble_values) + ")"
  
    
    # PaymentMethod
    payment_method = params.get("método de pago", None)
    posibble_values = ['transferencia bancaria' ,'tarjeta de crédito', 'cheque electrónico', 'cheque por correo']
    if(payment_method != None):
        try:
            value_index = posibble_values.index(payment_method)
            real_params.append(value_index)
            result_object['recognized']['método de pago'] = payment_method
        except ValueError:
            result_object['errors']['método de pago'] = "Método de pago solo puede ser " + " | ".join(posibble_values)
    else:
        result_object['missing']['método de pago'] = "Método de pago del cliente (" + " | ".join(posibble_values) + ")"
  
    
    # MonthlyCharges
    monthly_charges = params.get("cargos mensuales", None)
    if(monthly_charges != None):
        try:
            monthly_charges_number = float(monthly_charges)
            real_params.append(monthly_charges_number)
            result_object['recognized']['cargos mensuales'] = monthly_charges
        except ValueError:
            result_object['errors']['cargos mensuales'] = "Los cargos mensuales deben ser un número"
    else:
        result_object['missing']['cargos mensuales'] = "Los cargos mensuales del cliente (número)"

    
    # TotalCharges
    total_charges = params.get("cargos totales", None)
    if(total_charges != None):
        try:
            total_charges_number = float(total_charges)
            real_params.append(total_charges_number)
            result_object['recognized']['cargos totales'] = total_charges
        except ValueError:
            result_object['errors']['cargos totales'] = "Los cargos totales deben ser un número"
    else:
        result_object['missing']['cargos totales'] = "Los cargos totales del cliente (número)"


    if len(real_params) == 19:
        result_object['response'] = str(predictTelcoChurnModel([real_params]))

    return result_object