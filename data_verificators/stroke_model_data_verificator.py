from models.models import predictStrokeModel


def verify_data_stroke_model(params):
    real_params = []

    result_object = {
        "model": "stroke",
        "params": params,
        "recognized": {},
        "missing": {},
        "errors": {},
        "response": None
    }
    
    # gender
    gender = params.get("género", None)
    if(gender != None):
        if(gender in ['hombre','mujer']):
            real_params.append(1 if gender == 'hombre' else 0)
            result_object['recognized']['género'] = gender
        else:
            result_object['errors']['género'] = "Género solo puede ser hombre o mujer"
    else:
        result_object['missing']['género'] = "Género del paciente (hombre o mujer)"
    
    # age
    age = params.get("edad", None)
    if(age != None):
        try:
            age_number = int(age)
            real_params.append(age_number)
            result_object['recognized']['edad'] = age
        except ValueError:
            result_object['errors']['edad'] = "La edad debe ser un número natural"
    else:
        result_object['missing']['edad'] = "Edad del paciente en años"
    
    # hypertensión
    hyper = params.get("hipertensión", None)
    if(hyper != None):
        if(hyper in ['sí', 'no', 'si']):
            real_params.append(0 if hyper == 'no' else 1)
            result_object['recognized']['hipertensión'] = hyper
        else:
            result_object['errors']['hipertensión'] = "Hipertensión solo puede ser sí o no"
    else:
        result_object['missing']['hipertensión'] = "Si el paciente tiene hipertensión (sí o no)"
    
    # heart_disease
    heart_disease = params.get("enfermedad del corazón", None)
    if(heart_disease != None):
        if(heart_disease in ['sí', 'no', 'si']):
            real_params.append(0 if heart_disease == 'no' else 1)
            result_object['recognized']['enfermedad del corazón'] = heart_disease
        else:
            result_object['errors']['enfermedad del corazón'] = "Enfermedad del corazón solo puede ser sí o no"
    else:
        result_object['missing']['enfermedad del corazón'] = "Si el paciente tiene enfermedad del corazón (sí o no)"

    # ever_married
    ever_married = params.get("casado", None)
    if(ever_married != None):
        if(ever_married in ['sí', 'no', 'si']):
            real_params.append(0 if ever_married == 'no' else 1)
            result_object['recognized']['casado'] = ever_married
        else:
            result_object['errors']['casado'] = "Casado solo puede ser sí o no"
    else:
        result_object['missing']['casado'] = "Si el paciente alguna vez ha estado casado (sí o no)"

    # work_type
    work_type = params.get("tipo de trabajo", None)
    posibble_values = ['gobierno', 'nunca ha trabajado' ,'privado', 'independiente', 'niño']
    if(work_type != None):
        try:
            value_index = posibble_values.index(work_type)
            real_params.append(value_index)
            result_object['recognized']['tipo de trabajo'] = work_type
        except ValueError:
            result_object['errors']['tipo de trabajo'] = "Tipo de trabajo solo puede ser " + " | ".join(posibble_values)
    else:
        result_object['missing']['tipo de trabajo'] = "Tipo de trabajo del paciente (" + " | ".join(posibble_values) + ")"
    
    # residence_type
    residence_type = params.get("tipo de residencia", None)
    posibble_values = ['rural', 'urbana']
    if(residence_type != None):
        try:
            value_index = posibble_values.index(residence_type)
            real_params.append(value_index)
            result_object['recognized']['tipo de residencia'] = residence_type
        except ValueError:
            result_object['errors']['tipo de residencia'] = "Tipo de residencia solo puede ser " + " | ".join(posibble_values)
    else:
        result_object['missing']['tipo de residencia'] = "Tipo de residencia del paciente (" + " | ".join(posibble_values) + ")"

    # avg_glucose_level
    avg_glucose_level = params.get("nivel de glucosa promedio", None)
    if(avg_glucose_level != None):
        try:
            avg_glucose_level_number = float(avg_glucose_level)
            real_params.append(avg_glucose_level_number)
            result_object['recognized']['nivel de glucosa promedio'] = avg_glucose_level
        except ValueError:
            result_object['errors']['nivel de glucosa promedio'] = "El nivel de glucosa promedio debe ser un número"
    else:
        result_object['missing']['nivel de glucosa promedio'] = "El nivel de glucosa promedio del paciente (número)"

    # bmi
    bmi = params.get("índice de masa corporal", None)
    if(bmi != None):
        try:
            bmi_number = float(bmi)
            real_params.append(bmi_number)
            result_object['recognized']['índice de masa corporal'] = bmi
        except ValueError:
            result_object['errors']['índice de masa corporal'] = "El índice de masa corporal debe ser un número"
    else:
        result_object['missing']['índice de masa corporal'] = "El índice de masa corporal del paciente (número)"

    
    # smoking_status
    smoking_status = params.get("estado de fumado", None)
    posibble_values = ['desconocido', 'antes fumaba', 'nunca fumó', 'fuma']
    if(smoking_status != None):
        try:
            value_index = posibble_values.index(smoking_status)
            real_params.append(value_index)
            result_object['recognized']['estado de fumado'] = smoking_status
        except ValueError:
            result_object['errors']['estado de fumado'] = "Estado de fumado solo puede ser " + " | ".join(posibble_values)
    else:
        result_object['missing']['estado de fumado'] = "Estado de fumado del paciente (" + " | ".join(posibble_values) + ")"



    if len(real_params) == 10:
        result_object['response'] = str(predictStrokeModel([real_params]))

    return result_object