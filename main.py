import azure.cognitiveservices.speech as speechsdk
from flask import Flask, request
from flask_cors import CORS, cross_origin
from pydub import AudioSegment
from pydub.playback import play
import os
import time
import threading
import re
import json
from data_verificators.controller import main_controller
from face_recognition.face_recognition import get_face_classification


app = Flask(__name__)
CORS(app)


@app.route("/", methods=['POST'])
@cross_origin()
def main():
    request.files['audio'].save('temp/' + "temp")
    text = get_text_from_speach("temp")['text']

    model_and_params = what_they_want(text)

    previous_result = request.values.get('json', "{}")

    result_object = main_controller(model_and_params, json.loads(previous_result))


    return {
        "text": text,
        "result": result_object
    }

@app.route("/face", methods=['POST'])
@cross_origin()
def face():
    response = get_face_classification(request.files['image'])

    return response, 200 if response['error'] == False else 400
    


def get_text_from_speach(filename):
    exit_flag = threading.Event()

    sound = AudioSegment.from_file('temp/temp')
    sound.export('temp/temp.wav', 'wav')

    speech_config = speechsdk.SpeechConfig(
        subscription="d9312cd67a0a4cda84a82670627ca2d6", region="eastus")

    speech_config.speech_recognition_language = "es-CR"

    audio_input = speechsdk.AudioConfig(filename='temp/temp.wav')

    speech_recognizer = speechsdk.SpeechRecognizer(
        speech_config=speech_config, audio_config=audio_input, language="es-CR")

    text_recognized = ""

    def recognized(response):
        nonlocal text_recognized
        text_recognized += " " + response.result.text[:-1]

    speech_recognizer.recognized.connect(recognized)

    speech_recognizer.session_stopped.connect(lambda _: exit_flag.set())

    speech_recognizer.start_continuous_recognition()

    exited = 1

    if(exit_flag.wait(20)):
        exited = 0

    return {"exited": exited, "text": text_recognized}


def what_they_want(text):

    # Comprobar qué modelo quiere

    text = text.lower()
    text = text.replace(", ", " ")
    text = text.replace("eh", " ")
    text = text.replace("ah", " ")
    text = text.replace(" puntos ", ".")
    text = text.replace(" punto ", ".")
    text = text.replace(",", ".")
    text = text.split("datos")

    modelText = text[0]

    model = None

    if(modelText.find("aguacate") != -1):
        model = "avocado"
    elif(modelText.find("compañía telefónica") != -1):
        model = "telcom"
    elif(modelText.find("cerebro vascular") != -1 or modelText.find("cerebrovascular") != -1):
        model = "stroke"
    elif(modelText.find("cirrosis") != -1):
        model = "cirrhosis"
    elif(modelText.find("covid 19") != -1):
        model = "covid"
    elif(modelText.find("rossman") != -1):
        model = "rossman"
    elif(modelText.find("masa corporal") != -1):
        model = "imc"
    elif(modelText.find("hepatitis") != -1):
        model = "hepatitis"
    elif(modelText.find("bitcoin") != -1):
        model = "btc"
    elif(modelText.find("estándar") != -1):
        model = "standard_and_poors"

    # comprobar qué parámetros trae

    params = None
    params_object = {}
    if(len(text) > 1):
        text.pop(0)
        params = " ".join(text)

        params = params.split(" y ")

        params = list(map(lambda x: re.split(" es igual a | es ", x), params))


        for value in params:
            if(len(value) > 1):
                params_object[value[0].strip()] = value[1].strip()

    return {
        "model": model,
        "params": params_object
    }

if __name__ == "__main__":
    app.run(threaded=True, port=5000)



