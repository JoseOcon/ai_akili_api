import pickle
import pandas as pd
from models.read_rds_class import Model_regression_linear_multiple

models = {}

def importModels():
    global models
    models['stroke'] = pickle.load(open('models/stroke.sav', 'rb'))
    models['telcom'] = pickle.load(open('models/telco-churn.sav', 'rb'))
    models['cirrhosis'] = pickle.load(open('models/cirrhosis.sav', 'rb'))
    models['covid'] = pickle.load(open('models/recuperados-covid.sav', 'rb'))
    models['rossman'] = pickle.load(open('models/rossman-sales.sav', 'rb'))
    models['imc'] = Model_regression_linear_multiple().load('models/imc')
    models['hepatitis'] = pickle.load(open('models/hepatitis-c.sav', 'rb'))
    
    models['btc'] = pickle.load(open('models/btc-price.sav', 'rb'))
    models['avocado'] = pickle.load(open('models/avocado-price.sav', 'rb'))
    models['standard_and_poors'] = pickle.load(open('models/standard-and-poord.sav', 'rb'))


def predictStrokeModel(data):

    # gender
    # ['Male' 'Female' 'Other']
    # [1 0 2]

    # ever_married
    # ['Yes' 'No']
    # [1 0]

    # work_type
    # ['Private' 'Self-employed' 'Govt_job' 'children' 'Never_worked']
    # [2 3 0 4 1]

    # Residence_type
    # ['Urban' 'Rural']
    # [1 0]

    # smoking_status
    # ['formerly smoked' 'never smoked' 'smokes' 'Unknown']
    # [1 2 3 0]

    # ['gender', 'age', 'hypertension', 'heart_disease', 'ever_married',
    #  'work_type', 'Residence_type', 'avg_glucose_level', 'bmi',
    #  'smoking_status']

    return models['stroke'].predict(data)[0]

def predictTelcoChurnModel(data):

    # gender
    # ['Female' 'Male']
    # [0 1]

    # Partner
    # ['Yes' 'No']
    # [1 0]
    
    # Dependents
    # ['No' 'Yes']
    # [0 1]

    # PhoneService
    # ['No' 'Yes']
    # [0 1]

    # MultipleLines
    # ['No phone service' 'No' 'Yes']
    # [1 0 2]

    # InternetService
    # ['DSL' 'Fiber optic' 'No']
    # [0 1 2]

    # OnlineSecurity
    # ['No' 'Yes' 'No internet service']
    # [0 2 1]

    # OnlineBackup
    # ['Yes' 'No' 'No internet service']
    # [2 0 1]

    # DeviceProtection
    # ['No' 'Yes' 'No internet service']
    # [0 2 1]

    # TechSupport
    # ['No' 'Yes' 'No internet service']
    # [0 2 1]

    # StreamingTV
    # ['No' 'Yes' 'No internet service']
    # [0 2 1]

    # StreamingMovies
    # ['No' 'Yes' 'No internet service']
    # [0 2 1]

    # Contract
    # ['Month-to-month' 'One year' 'Two year']
    # [0 1 2]

    # PaperlessBilling
    # ['Yes' 'No']
    # [1 0]

    # PaymentMethod
    # ['Electronic check' 'Mailed check' 'Bank transfer (automatic)'
    # 'Credit card (automatic)']
    # [2 3 0 1]

    # ['gender', 'SeniorCitizen', 'Partner', 'Dependents',
    #    'tenure', 'PhoneService', 'MultipleLines', 'InternetService',
    #    'OnlineSecurity', 'OnlineBackup', 'DeviceProtection', 'TechSupport',
    #    'StreamingTV', 'StreamingMovies', 'Contract', 'PaperlessBilling',
    #    'PaymentMethod', 'MonthlyCharges', 'TotalCharges']

    return models['telco-churn'].predict(data)[0]

def predictCirrhosisModel(data):

    # Status
    # ['C' 'CL' 'D']
    # [0 1 2]

    # Drug
    # ['D-penicillamine' 'Placebo']
    # [0 1]

    # Sex
    # ['F' 'M']
    # [0 1]

    # Ascites
    # ['N' 'Y']
    # [0 1]

    # Hepatomegaly
    # ['N' 'Y']
    # [0 1]

    # Spiders
    # ['N' 'Y']
    # [0 1]

    # Edema
    # ['N' 'S' 'Y']
    # [0 1 2]

    # ['N_Days', 'Status', 'Drug', 'Age', 'Sex', 'Ascites', 'Hepatomegaly',
    #    'Spiders', 'Edema', 'Bilirubin', 'Cholesterol', 'Albumin', 'Copper',
    #    'Alk_Phos', 'SGOT', 'Tryglicerides', 'Platelets', 'Prothrombin']

    return models['cirrhosis'].predict(data)[0]

def predictCovidModel(data):
    #date
    print("Fecha reconocida:", data)
    prediction = models['covid'].get_prediction(start=pd.to_datetime(data[0][0]), dynamic=False)
    return prediction.predicted_mean[0]

def predictRossmanModel(data):
    #date
    print("Fecha reconocida:", data)
    prediction = models['rossman'].get_prediction(start=pd.to_datetime(data[0][0]), dynamic=False)
    return prediction.predicted_mean[0]

def predictBtcModel(data):
    #date
    print("Fecha reconocida:", data)
    prediction = models['btc'].get_prediction(start=pd.to_datetime(data[0][0]), dynamic=False)
    return prediction.predicted_mean[0]

def predictAvocadoModel(data):
    #date
    print("Fecha reconocida:", data)
    prediction = models['avocado'].get_prediction(start=pd.to_datetime(data[0][0]), dynamic=False)
    return prediction.predicted_mean[0]


def predictStandardAndPoorsModel(data):
    #date
    print("Fecha reconocida:", data)
    prediction = models['standard_and_poors'].get_prediction(start=pd.to_datetime(data[0][0]), dynamic=False)
    return prediction.predicted_mean[0]


def predictIMCModel(data):
    #Abdomen: flt
    #Height: flt
    #Hip: flt
    #Thigh: flt
    #Weight: flt

    prediction = models['imc'].predict(data)
    return prediction

def predictHepatitisModel(data):
    # Age Num
    # Sex F=1 M=2
    # ALB flt
    # ALP flt
    # ALT flt
    # AST flt 
    # BIL flt 
    # CHE flt 
    # CHOL flt
    # CREA flt
    # GGT flt
    # PROT flt

    print(data)
    prediction = models['hepatitis'].predict(data)[0]
    print(prediction)
    return prediction


importModels()

#print(predictStrokeModel([[1, 12, 1, 0, 1, 1, 1, 290, 30, 1]]))