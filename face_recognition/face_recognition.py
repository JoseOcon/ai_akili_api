import asyncio
import io
import glob
import os
import sys
import time
import uuid
import requests
from urllib.parse import urlparse
from io import BytesIO
import json
# To install this module, run:
# python -m pip install Pillow
from PIL import Image, ImageDraw
from azure.cognitiveservices.vision.face import FaceClient
from msrest.authentication import CognitiveServicesCredentials
from azure.cognitiveservices.vision.face.models import TrainingStatusType, Person

def get_face_classification(file):
    try:
        # This key will serve all examples in this document.
        KEY = "3ecd7bf605b444338d3bcc48e3e811d1"

        # This endpoint will be used in all examples in this quickstart.
        ENDPOINT = "https://faceclassificator.cognitiveservices.azure.com"

        # Create an authenticated FaceClient.
        face_client = FaceClient(ENDPOINT, CognitiveServicesCredentials(KEY))

        # We use detection model 3 to get better performance.
        detected_faces = face_client.face.detect_with_stream(file, return_face_attributes=['emotion'])
        if not detected_faces:
            return {
                "error": True,
                "message": "Face not found on image"
            }

        # Display the detected face ID in the first single-face image.
        # Face IDs are used for comparison to faces (their IDs) detected in other images.
        faces = []
        for face in detected_faces: faces.append({"face_rectangle": face.face_rectangle.__dict__, "face_emotions": face.face_attributes.emotion.__dict__})

        return {
            "error": False,
            "detected_faces": faces
        }
    except Exception as e: 
        print(e)
        return {
            "error": True,
            "message": "Unexpected error"
        }

